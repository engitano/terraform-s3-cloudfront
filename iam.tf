data "template_file" "deployer_role_policy_file" {
  template = "${file("${path.module}/deployer_role_policy.json")}"

  vars {
    bucket = "${var.bucket_name}"
    cloudfront_arn = "${aws_cloudfront_distribution.website_cdn.arn}"
  }
}

resource "aws_iam_user" "deployer" {
  name = "${var.deployer_role_name}"
}

resource "aws_iam_user_policy" "deployer" {
  name = "${var.deployer_role_name}-policy"
  user = "${aws_iam_user.deployer.name}"

  policy = "${data.template_file.deployer_role_policy_file.rendered}"
}

resource "aws_iam_access_key" "deployer" {
  user = "${aws_iam_user.deployer.name}"
}
