resource "aws_s3_bucket" "website_bucket" {
  bucket   = "${var.bucket_name}"
  policy   = "${data.template_file.bucket_policy.rendered}"

  website {
    index_document = "index.html"
  }
}
data "template_file" "bucket_policy" {
  template = "${file("${path.module}/website_bucket_policy.json")}"

  vars {
    bucket = "${var.bucket_name}"
    secret = "${var.duplicate_content_penalty_secret}"
  }
}
