variable "aws_region" {}

variable "bucket_name" {}

variable "index_document" {
  default = "index.html"
}

variable "error_document" {
  default = ""
}

variable "routing_rules" {
  default = ""
}

variable "domains" {
  type = "list"
  description = "A list of your domains 'github.com', 'www.github.com'"
}

variable "dns_zone_id" {
  description = "Your Route 53 Zone Id"
}

variable "price_class" {
  default = "PriceClass_All"
  description = "Cloudfront Price Class"
}

variable "duplicate_content_penalty_secret" {}

variable "default_root_object" {
  default = ""
}

variable "deployer_role_name" {}