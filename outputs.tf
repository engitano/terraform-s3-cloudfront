output "deployer_access_id" {
  value = "${aws_iam_access_key.deployer.id}"
}

output "deployer_secret_access_key" {
  value = "${aws_iam_access_key.deployer.secret}"
}

output "cloudfront_distro_id" {
  value = "${aws_cloudfront_distribution.website_cdn.id}"
}
