resource "aws_route53_record" "subdomains" {
  count = "${length(var.domains)}"
  zone_id = "${var.dns_zone_id}"
  name    = "${var.domains[count.index]}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.website_cdn.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.website_cdn.hosted_zone_id}"
    evaluate_target_health = true
  }
}
