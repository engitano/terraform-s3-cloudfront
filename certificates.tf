provider "aws" {
  alias  = "certs"
  region = "us-east-1"
}
resource "aws_acm_certificate" "wildcard_with_san" {
  provider = "aws.certs"
  domain_name = "${element(var.domains,0)}"
  validation_method = "DNS"
  subject_alternative_names  = ["${slice(var.domains, 1, length(var.domains))}"]
}

resource "aws_route53_record" "cert_with_san_validation" {
  name = "${aws_acm_certificate.wildcard_with_san.domain_validation_options.0.resource_record_name}"
  type = "${aws_acm_certificate.wildcard_with_san.domain_validation_options.0.resource_record_type}"
  zone_id = "${var.dns_zone_id}"
  records = ["${aws_acm_certificate.wildcard_with_san.domain_validation_options.0.resource_record_value}"]
  ttl = 60
}

resource "aws_acm_certificate_validation" "wildcard_with_san" {
  provider = "aws.certs"
  certificate_arn = "${aws_acm_certificate.wildcard_with_san.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert_with_san_validation.fqdn}"]
}
